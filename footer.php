	<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="inline-block">
                         <a href="https://twitter.com/pbwebdev?lang=en">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </p>
                    <p class="inline-block">
                            <a href="https://www.facebook.com/pbwebdev/">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                    </p>
                    <p class="inline-block">

                            <a href="https://plus.google.com/113245780922243453082">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                    </p>
                    <p class="inline-block">

                            <a href="https://www.linkedin.com/company-beta/1226736/?pathWildcard=1226736">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                    </p>
                    <div class="col-xs-12 copyright">
                    © Copyright <?php echo date('Y'); ?> <a href="<?php echo home_url(); ?> " title="Front End Skills Test">Front End Skills Test</a> - Experience by <a href="https://pbwebdev.com/" title="Joomla web experts sydney">PB Web Development</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<?php wp_footer(); ?>
</body>
</html>