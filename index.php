<?php get_header(); ?>
    <header class="intro-header" data-position="top" data-parallax="scroll" data-bleed="10" data-image-src="<?php echo get_template_directory_uri(); ?>/img/home-bg.jpg" data-natural-width="1024" data-natural-height="512">
        <div class="container">
            <div class="row">
                <div">
                    <div class="site-heading">
                        <h1>Clean Blog</h1>
                        <hr class="small">
                        <span class="subheading">A Clean Blog Theme by Start Bootstrap</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container" style="margin-top: 5%;">
        <div class="row">
            <div class="col-sm-10 col-md-8 col-lg-8">
                            
                    <?php 
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array( 'post_type' => 'post', 'posts_per_page' => 2, 'paged' => $paged, 'order'=>'ASC' );
                        $wp_query = new WP_Query($args); 
                    ?>

                    <div class="post-preview">

                        <?php while ( have_posts() ) : the_post(); ?>
                            <a href="<?php the_permalink(); ?>">
                                <h2 class="post-title"><?php the_title(); ?></h2>
                                <h3 class="post-subtitle"><?php the_excerpt(); ?></h3>  
                            </a>
                            <p class="post-meta">Posted by <a href="#"><?php the_author(); ?></a> on <em><?php the_time('F j, Y') ?></em></p>
                            <hr>
                        <?php endwhile; ?>
                    </div>

                
                <ul class="pager">
                    <li class="next">
                        <?php next_posts_link( '&larr; Older posts', $wp_query ->max_num_pages); ?>
                        <?php previous_posts_link( 'Newer posts &rarr;' ); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <hr>
<?php get_footer(); ?>

