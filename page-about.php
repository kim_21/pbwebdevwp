<?php
/*
    Template Name: About Us Template
  */
?>
<?php get_header(); ?>
<?php
    if (have_posts()) :
    while (have_posts()) :
    the_post(); 
?>
<header class="intro-header" data-position="top" data-parallax="scroll" data-bleed="10" data-image-src="<?php echo get_template_directory_uri(); ?>/img/about-bg.jpg" data-natural-width="1024" data-natural-height="512">
        <div class="container">
            <div class="row">
                <div">
                    <div class="site-heading">
                        <h1><?php the_title(); ?></h1>
                        <hr class="small">
                        <span class="subheading"><?php the_subtitle(); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
<div class="container" style="margin-top: 5%;">
    <div class="row">
        <div class="col-md-12">
			<?php the_content(); ?>
        </div>
    </div>
</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>