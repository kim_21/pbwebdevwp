<?php
/*
    Template Name: Sample Post Template
  */
?>
<?php get_header(); ?>
<?php  
$post_id = 17;
$queried_post = get_post($post_id);
$title = $queried_post->post_title;
$excerpt = $queried_post->post_excerpt;
$author = $queried_post->post_author;
$date = $queried_post->$post_date;
?>
<header class="intro-header" data-position="top" data-parallax="scroll" data-bleed="10" data-image-src="<?php echo get_template_directory_uri(); ?>/img/post-bg.jpg" data-natural-width="1024" data-natural-height="512">
    <div class="container">
        <div class="row">
            <div>
                <div class="post-heading">
                    <h1><?php echo $title;  ?></h1>
                    <h2 class="subheading"><?php echo $excerpt; ?></h2>

                    <span class="meta">Posted by <a href="#"><?php the_author_meta( 'user_nicename' , $author ); ?> </a> on 
                    <?php echo get_the_date();?></span>
                </div>
            </div>
        </div>
    </div>
</header>
<article class="margin5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $queried_post->post_content; ?>
                </div>
            </div>
        </div>
    </article>
<?php get_footer(); ?>