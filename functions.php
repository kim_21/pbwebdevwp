<?php 
	
	function register_stylesheet(){
		wp_enqueue_style( 'style-sheet', get_stylesheet_uri() );
		wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
		wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );		
	}
	add_action( 'wp_enqueue_scripts', 'register_stylesheet' );

	function register_scripts() {
	    wp_enqueue_script( 'jquery-2', get_template_directory_uri().'/js/jquery.js', array(), null, true );
	    wp_enqueue_script( 'bootstrap-script', get_template_directory_uri().'/js/bootstrap.min.js', array( 'jquery-2' ), true );
	    wp_enqueue_script( 'clean-blog-script', get_template_directory_uri().'/js/clean-blog.min.js', array( 'jquery-2' ), true );
	    wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'parallax-script', get_template_directory_uri().'/js/parallax.min.js', array( 'jquery' ), true );
	}
	add_action('wp_enqueue_scripts', 'register_scripts');

	function menu() {
	  register_nav_menus(
	    array(
	      'header-menu' => __( 'Header Menu' ),
	      'footer-menu' => __( 'Footer Menu' ),
	      'footer-second-menu' => __( 'Footer Second Menu' ),
	      'header-second-menu' => __( 'Header Second Menu' ),
	      'social-media-menu' => __( 'Social Media Menu' ),
	      'mobile-view-menu' => __( 'Mobile View Menu' )
	    )
	  );
	}
	add_action( 'init', 'menu' );
 ?>