<?php
/*
    Template Name: Contact Template
  */
?>
<?php get_header(); ?>
<?php
    if (have_posts()) :
    while (have_posts()) :
    the_post(); 
?>
<header class="intro-header" data-position="top" data-parallax="scroll" data-bleed="10" data-image-src="<?php echo get_template_directory_uri(); ?>/img/contact-bg.jpg" data-natural-width="1024" data-natural-height="512">
    <div class="container">
        <div class="row">
            <div>
                <div class="post-heading">
                    <h1><?php the_title();  ?></h1>
                    <h2 class="subheading"><?php the_excerpt(); ?></h2>

                    <span class="meta">Posted by <a href="#"><?php the_author(); ?> </a> on 
                    <?php echo get_the_date();?></span>

                </div>
            </div>
        </div>
    </div>
</header>
<?php endwhile; endif; ?>
<article class="margin5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <?php  
                        $post_id = 30;
                        $queried_post = get_post($post_id);
                        $content = $queried_post->post_content;
                        echo $content;
                    ?>
                </div>
            </div>
        </div>
    </article>
<?php get_footer(); ?>